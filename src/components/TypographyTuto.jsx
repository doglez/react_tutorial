import React from 'react'
import Typography from '@material-ui/core/Typography'
import 'fontsource-roboto';


function TypographyTuto() {
    return (
        <div>
            <Typography variant="h2" color="initial">Hola</Typography>
            <Typography variant="body1" color="secondary" align='right'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore incidunt rem sed! Asperiores similique, ut nesciunt, iure facilis totam magni ab velit quo deserunt laborum accusantium aspernatur! Facilis, cum dicta.
            Obcaecati velit eligendi architecto natus dignissimos. Autem harum illum mollitia, voluptates repudiandae iste fugiat consequatur odit magnam maxime fugit eveniet atque sapiente? Ipsum maxime impedit tenetur magnam quos, inventore voluptatibus!
            Voluptatum at nostrum nam accusamus nisi vel magni provident illo placeat odio facilis ullam accusantium eum itaque, quo possimus aliquam blanditiis harum delectus! Consectetur deleniti vero voluptatum! Officiis, ea vitae.
            Rem autem quam facere quis facilis iure corporis accusantium odit nam dolorem saepe quas sunt, eos atque magnam a nesciunt vitae placeat rerum nostrum at quisquam assumenda! Incidunt, numquam minus?
            Ipsum voluptatibus deserunt perferendis alias asperiores iusto et aspernatur odio libero. Officia fugiat rerum perferendis ut. Maiores facilis optio, adipisci error eius, laboriosam, soluta assumenda id nostrum placeat alias veniam?
            Eligendi nostrum hic dolores aperiam facere ad doloremque cupiditate sunt, ullam officia voluptas optio assumenda accusamus, soluta doloribus numquam a! Optio perferendis illum eos, ea rem quasi quos perspiciatis quis!
            Amet consequatur eos ab, eligendi repellendus quam, ea saepe numquam minima dolore tempore perspiciatis facilis excepturi sint, cum architecto a. Perspiciatis temporibus voluptatem illo ullam! Sunt velit voluptas ex eum?
            Odit, rerum perferendis provident vero ratione laborum quo, sint sapiente asperiores natus magnam illum nisi blanditiis nam culpa iste consequuntur quibusdam nihil labore aliquid sunt? Laboriosam dolorum deleniti iusto placeat?
            Magnam quia nesciunt aut libero quidem dolorum, corporis placeat quo officiis temporibus reiciendis molestiae, quibusdam autem magni minima repudiandae laboriosam minus atque, obcaecati nostrum necessitatibus quos. Nemo molestias harum autem!
            Voluptas nostrum sapiente at rerum tempore, magni nihil facilis voluptatum quis delectus quae impedit fuga officiis veniam eos eveniet voluptates saepe quod recusandae error beatae provident dignissimos harum? Consectetur, quo.
            Aperiam quam inventore eaque reprehenderit quod tenetur, ex facilis odit laudantium enim a delectus consequuntur illum necessitatibus doloremque cum omnis neque tempora iure architecto sit accusantium esse soluta animi? Voluptate?
            Expedita architecto magni unde quaerat modi doloribus sit sequi quod assumenda corrupti illum rem debitis soluta recusandae possimus quo optio accusantium, nulla dolor suscipit laboriosam velit consequuntur eveniet. Laborum, dolores.
            Earum magni corrupti ipsum, ratione laboriosam quasi officiis at quidem consequuntur, repudiandae similique est eos neque iure consequatur! Ea laboriosam voluptatem totam porro vitae amet veritatis commodi repudiandae assumenda a?
            Minima quae ratione quo sunt enim voluptate fugiat illo odit accusantium veniam, debitis dolor. Quaerat accusantium excepturi, eos commodi necessitatibus libero voluptate, modi magnam fuga quas dolores similique at cum?
            Nulla expedita aut enim nam optio eum cumque harum ab! Officia, magnam saepe ipsam eos rerum ad inventore voluptatem sunt nisi esse! Aliquid atque perferendis corrupti, ipsa pariatur aliquam nulla.
            Nostrum non, est hic neque corrupti praesentium eveniet nisi eos culpa sunt tenetur consequatur iusto facere labore vel soluta dolore amet. Pariatur laboriosam, exercitationem amet itaque ratione quasi ducimus delectus!
            Omnis libero perferendis quia odit totam? Nemo doloribus voluptatibus, laboriosam laudantium accusantium consequuntur pariatur dicta harum commodi, praesentium ducimus enim eveniet incidunt adipisci. Aliquid rem quis assumenda porro ex officia.
            Fugiat expedita non harum eos reprehenderit, pariatur nam voluptates totam magni possimus, aliquid, quo reiciendis ea asperiores quidem. Doloremque eaque maxime recusandae at animi quis unde qui itaque saepe dolorem?
            Adipisci, perspiciatis soluta voluptate saepe similique corporis magnam praesentium ex facilis totam dignissimos earum! Animi obcaecati consequuntur perferendis, saepe officiis, maxime, sunt deserunt nam magni neque recusandae. Molestiae, quam quia!
            Aut accusamus labore neque distinctio ad unde, error corporis sequi sed, et quisquam! Facilis voluptatibus maxime laboriosam labore, magni dicta soluta, voluptatum nemo esse autem consequatur amet eum vel totam!
            Perspiciatis voluptatibus omnis iure ratione at, voluptatum cumque mollitia. Quod cum commodi aperiam fugiat quaerat, facere placeat illo laborum odio minus aspernatur, voluptate quidem adipisci voluptatum reprehenderit veniam eveniet ipsa?
            Expedita hic sint voluptas, pariatur architecto nam numquam commodi facilis accusantium voluptates quidem autem ullam provident error asperiores cum quis fuga a animi? Animi velit ab sint ipsum dolores quos.
            Dolorem, tempora pariatur! Laudantium quia sunt id consectetur, facilis aut, animi veniam at iusto itaque alias? Ad harum voluptatibus at doloremque maxime recusandae hic modi maiores accusamus, enim veritatis libero.
            Nihil sit optio nam ad necessitatibus! Corrupti tenetur minus ea iure, voluptatem vero nobis sint aliquid libero atque repudiandae ipsam enim velit vel architecto necessitatibus consequatur tempora laborum nulla. Delectus!
            Quae repudiandae laborum libero ex enim, necessitatibus cupiditate, iure nesciunt ea, quia at vero eum. Quis quaerat quibusdam voluptate fugit minima deleniti unde cupiditate aliquam amet voluptas? Excepturi, cum nostrum?
            Ipsa porro architecto sequi aliquam dolore dolor, dignissimos eum culpa odio harum nesciunt officiis quis necessitatibus, blanditiis corporis optio? Deleniti, illo necessitatibus. Nisi repellendus temporibus asperiores rem suscipit et numquam.
            Blanditiis, cumque. Sequi ipsum quisquam officia! Necessitatibus dolorum repudiandae eaque quae omnis corrupti, nihil provident aliquid explicabo beatae voluptatibus doloribus alias debitis facere qui voluptatum praesentium? Praesentium earum id consequuntur.
            Delectus commodi hic laudantium dignissimos ad error sunt eum fuga veniam maiores dolores est quo, dolorem itaque, quis voluptatem perspiciatis! Eius, cumque dolores. Nostrum ipsam officia voluptatem voluptates, vel pariatur!
            Odio harum hic quidem alias consequuntur? Est, id ea. Harum labore quo ducimus nulla iste pariatur similique laboriosam libero praesentium, sunt minus dignissimos. Eius saepe in iste quaerat officia illo.
            Facilis quam cum hic quaerat ab quia vitae, eligendi commodi saepe aliquid modi laudantium impedit? Earum repellendus, quam velit nobis et commodi at culpa, excepturi similique, eius mollitia consectetur nam!</Typography>
            <hr/>
        </div>
    )
}

export default TypographyTuto
