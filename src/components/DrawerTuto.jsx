import React from 'react'
import { Divider, Drawer, makeStyles } from "@material-ui/core";
import ListsTuto from './ListsTuto'

const drawerWidth = 240;

const useStyle = makeStyles(theme => ({
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
}))

const DrawerTuto = (props) => {

    const classes = useStyle();

    return (
        <Drawer 
            className={classes.drawer} 
            classes={{paper: classes.drawerPaper,}}
            anchor= 'left'
            variant={props.variant}
            open={props.open}
            onClose={props.onClose ? props.onClose : null}
        >

            <div className={classes.toolbar}></div>
            <Divider/>
            <ListsTuto/>
        </Drawer>
    )
}

export default DrawerTuto
