import React from 'react'
import { Box, Grid } from "@material-ui/core";

const BoxTuto = () => {
    return (
        <div>
            <Box>hola</Box>
            <Box
                color= 'secondary.contrastText'
                bgcolor='secondary.main'
                border={2}
                borderColor='yellow'
                boxShadow={3}
            >adios</Box>
            <Box
                color= 'primary.contrastText'
                bgcolor='primary.main'
                mt={2}
                mr={4}
                ml={8}
                p={5}
            >adios</Box>

            <Grid container>
                <Grid item xs={12} sm={6}>
                    <Box border={2} my={2}>item 1</Box>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Box border={2} my={2}>item 1</Box>
                </Grid>
            </Grid>
        </div>
    )
}

export default BoxTuto
