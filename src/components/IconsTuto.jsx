import { Delete } from '@material-ui/icons'
import React from 'react'
import { Button } from '@material-ui/core'

function IconsTuto() {
    return (
        <div>
            <Delete color='primary' fontSize='large' titleAccess='basurero'/>
            <Button
              variant="contained"
              color="primary"
              startIcon={<Delete color='secondary'/>}
              
            >Borrar
              
            </Button>

            <Button
              variant="contained"
              color="primary"
              endIcon={<Delete color='secondary'/>}
              
            >Borrar
              
            </Button>

            <hr/>
        </div>
    )
}

export default IconsTuto
