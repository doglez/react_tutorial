import React from 'react'
import { withWidth, Typography, Hidden, Button } from "@material-ui/core";

const HiddenTuto = (props) => {
    return (
        <div>
            <Typography variant="h6" color="initial">
                Ancho: {props.width}
            </Typography>
            <Hidden smDown>
                <Button variant="contained" color="secondary">
                  SM
                </Button>
            </Hidden>
            <Hidden mdUp>
                <Button variant="contained" color="secondary">
                  MD
                </Button>
            </Hidden>
        </div>
    )
}

export default withWidth()(HiddenTuto)
