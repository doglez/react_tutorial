import { createMuiTheme } from '@material-ui/core'

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#212121', 
            light: '#484848',
            dark: '#000000'
        },
        secondary: {
            main: '#d50000', 
            light: '#ff5131',
            dark: '#9b0000'
        },
        background: {
            default: '#64b5f6',
        }
    },
    typography: {
        fontFamily: "'Hachi Maru Pop', cursive"
    },    
})

export default theme
