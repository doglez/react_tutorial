import React from 'react'
import {Button} from '@material-ui/core'

function ButtonTuto() {
    return (
        <div>
         <Button variant="contained" color="secondary">
          hola
         </Button>
         <Button variant="outlined" color="primary" href='https://google.com' target="_blank">
          Google
         </Button>
         <Button variant="contained" color="default" disabled>
           casa
         </Button>
         <hr/>
        </div>
    )
}

export default ButtonTuto
