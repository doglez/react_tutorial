import React from 'react'
import { Hidden, makeStyles } from "@material-ui/core";
import NavBar from './NavBar'
import DrawerTuto from './DrawerTuto';
import BoxTuto from './BoxTuto';

const useStyle = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        background: theme.palette.background.default,
        padding: theme.spacing(3)
    }
}))

const MainTuto = (props) => {

    const classes = useStyle();
    const [mobilOpen, setMobilOpen] = React.useState(false)
    const handleDrawerToggle = () =>{
        setMobilOpen(!mobilOpen)
    }

    return (
        <div className={classes.root}>
            <NavBar handleDrawerToggle={handleDrawerToggle}/>
            <Hidden smUp>
                <DrawerTuto
                    variant='temporary'
                    open={mobilOpen}
                    onClose={handleDrawerToggle}
                />
            </Hidden>

            <Hidden xsDown>
                <DrawerTuto
                    variant= 'permanent'
                    open={true}
                />
            </Hidden>
            <div className={classes.content}>
                <div className={classes.toolbar}></div>
                <BoxTuto/>
            </div>
        </div>
    )
}

export default MainTuto
