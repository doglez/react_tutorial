import React from 'react'
import {AppBar, makeStyles, Toolbar, Typography, IconButton} from '@material-ui/core'
import MenuIcon from '@material-ui/icons/Menu';

const drawerWidth = 240;

const useStyle = makeStyles(theme => ({        
    menuButton: {
        marginRight: theme.spacing(2),    
        [theme.breakpoints.up('sm')]: {
          display: 'none',
        }
    },
    title: {
        flexGrow: 1,
    },
    appBar: {
      [theme.breakpoints.up('sm')]: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
      },
    }
}))

function NavBar(props) {
    const classes = useStyle()
    return (        
      <AppBar className={classes.appBar}>
        <Toolbar>
          <IconButton aria-label="menu" className={classes.menuButton} color='inherit' onClick={()=> props.handleDrawerToggle()}>
          <MenuIcon/>
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            />oglez
          </Typography>
        </Toolbar>
      </AppBar>
    )
}

export default NavBar

