import React from 'react'
import { Divider, List, ListItem, ListItemIcon, ListItemText } from "@material-ui/core";
import { CloudQueue, HomeOutlined, InfoOutlined } from "@material-ui/icons";

const ListsTuto = () => {
    return (
        <div>
            <List component='nav'>
                <ListItem button>
                    <ListItemIcon>
                        <HomeOutlined/>
                    </ListItemIcon>
                    <ListItemText primary='Home'/>
                </ListItem>

                <ListItem button>
                    <ListItemIcon>
                        <InfoOutlined/>
                    </ListItemIcon>
                    <ListItemText primary='Info'/>
                </ListItem>

                <ListItem button>
                    <ListItemIcon>
                        <CloudQueue/>
                    </ListItemIcon>
                    <ListItemText primary='Cloud'/>
                </ListItem>
                <Divider/>
            </List>
        </div>
    )
}

export default ListsTuto
