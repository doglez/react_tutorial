import { ThemeProvider } from '@material-ui/core'
import React from 'react'
import MainTuto from './components/MainTuto'
import theme from './components/ThemeConfig'

function App() {
    return (
        <ThemeProvider theme={theme}>
            <MainTuto/>           
        </ThemeProvider>
    )
}

export default App
